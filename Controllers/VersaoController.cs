﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Desafio01.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VersaoController : Controller
    {
        [HttpGet]
        public string  Get()
        {
            return "versao 04";
        }
    }
}
