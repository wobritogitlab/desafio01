﻿pipeline {
    agent any

    environment {
		registry = "wobritodevops/desafio01"
        registryCredential = "dockerhub_id"
        dockerImage = ''
    }

    stages {
    	stage('Copiando Repositorio') {
    		steps {  
                git url: 'https://gitlab.com/wobritogitlab/desafio01.git'
			}
    	}
    	stage('Montando a Imagem Docker') {
            steps{
                script {
                    dockerImage = docker.build registry + ":develop"
                }
            }
        }
        stage('Enviando a imagem para o Docker Hub') {
            steps{
                script {
                    docker.withRegistry( '', registryCredential) {
                        dockerImage.push()
                    }
                }
            }
        }
    	stage('Limpando as imagens docker') {
        	steps {
            	sh "docker rmi $registry:develop"
        	}
		}
    }
}